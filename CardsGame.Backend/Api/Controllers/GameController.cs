﻿using System;
using Microsoft.AspNetCore.Mvc;
using Services.Abstraction;
using Services.Requests;

namespace Api.Controllers
{
    public class GameController : BaseController
    {
        private readonly IGameService _gameService;

        public GameController(IGameService gameService)
        {
            _gameService = gameService;
        }

        [HttpPost("/game")]
        public IActionResult NewGame([FromBody] StartGameRequest request)
        {
            return ProcessRequest(() => _gameService.StartGame(request));
        }

        [HttpPost("/game/{gameId}/draw")]
        public IActionResult Draw(Guid gameId, [FromBody] DrawRequest request)
        {
            return ProcessRequest(() => _gameService.Draw(gameId, request));
        }
    }
}