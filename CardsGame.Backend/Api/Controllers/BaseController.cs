﻿using System;
using Microsoft.AspNetCore.Mvc;
using Model.Exceptions;

namespace Api.Controllers
{
    public class BaseController : Controller
    {
        protected IActionResult ProcessRequest(Func<object> action)
        {
            try
            {
                var result = action();
                return Ok(result);
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
            catch (BadRequestException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}