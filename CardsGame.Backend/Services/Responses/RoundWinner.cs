﻿using System;

namespace Services.Responses
{
    public class RoundWinner
    {
        public Guid PlayerId { get; set; }
        public Guid CardId { get; set; }
    }
}