﻿using System;
using System.Collections.Generic;
using Model;

namespace Services.Responses
{
    public class DrawResponse
    {
        public Guid GameId { get; set; }

        public Guid PlayerId { get; set; }

        public Card Card { get; set; }

        public int Round { get; set; }

        public IEnumerable<RoundWinner> Winners { get; set; }
    }
}