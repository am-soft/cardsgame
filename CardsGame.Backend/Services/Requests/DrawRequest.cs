using System;
using Model;

namespace Services.Requests
{
    public class DrawRequest
    {
        public Guid PlayerId { get; set; }
        public CardType CardType { get; set; }
    }
}