﻿using System.Collections.Generic;

namespace Services.Requests
{
    public class StartGameRequest
    {
        public IEnumerable<string> Players { get; set; }
    }
}