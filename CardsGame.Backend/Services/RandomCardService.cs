﻿using System;
using System.Linq;
using Model;
using Services.Abstraction;
using Storage.Repository;
using Attribute = Model.Attribute;

namespace Services
{
    public class RandomCardService : IRandomCardService
    {
        private readonly IRepository<Card> _cardRepository;
        private readonly IRepository<Draw> _drawRepository;
        private readonly IRepository<Attribute> _attributesRepository;

        public RandomCardService(IRepository<Card> cardRepository, IRepository<Draw> drawRepository, IRepository<Attribute> attributesRepository)
        {
            _cardRepository = cardRepository;
            _drawRepository = drawRepository;
            _attributesRepository = attributesRepository;
        }

        public Card GetRandomCard(Game game, int currentRound, CardType cardType)
        {
            var alreadyDrawCards = _drawRepository
                .Query()
                .Where(x => x.GameId == game.Id && x.Round == currentRound)
                .ToList();

            var randomCard = _cardRepository.Query()
                .Where(x => x.CardType == cardType && alreadyDrawCards.All(y => y.CardId != x.Id))
                .OrderBy(card => Guid.NewGuid())
                .First();

            randomCard.Attribute = _attributesRepository.Get(randomCard.AttributeId);

            return randomCard;
        }
    }
}