﻿using System;
using System.Linq;
using Model;
using Model.Exceptions;
using Serilog;
using Services.Abstraction;
using Services.Requests;
using Services.Responses;
using Storage.Repository;

namespace Services
{
    public class GameService : IGameService
    {
        private readonly ILogger _logger;
        private readonly IRepository<Game> _gameRepository;
        private readonly IRandomCardService _randomCardService;
        private readonly IPlayersService _playersService;
        private readonly IDrawService _drawService;
        private readonly IScoreService _scoreService;

        public GameService(ILogger logger, IRepository<Game> gameRepository, IRandomCardService randomCardService, IPlayersService playersService, IDrawService drawService, IScoreService scoreService)
        {
            _logger = logger;
            _gameRepository = gameRepository;
            _randomCardService = randomCardService;
            _playersService = playersService;
            _drawService = drawService;
            _scoreService = scoreService;
        }

        public Game StartGame(StartGameRequest request)
        {
            if (request?.Players == null || !request.Players.Any())
                throw new BadRequestException("No players in Start Game Request");

            var game = new Game { PlayersCount = request.Players.Count() };
            _gameRepository.Insert(game);

            var players = _playersService.AddGamePlayers(game, request.Players);

            _logger.Information($"New game started, ID: {game.Id}. Players count: {players.Count()}");

            return game;
        }

        public DrawResponse Draw(Guid gameId, DrawRequest request)
        {
            if (request == null)
                throw new BadRequestException("DrawRequest is invalid");

            var game = _gameRepository.Get(gameId);
            var player = _playersService.GetGamePlayer(game, request.PlayerId);
            var currentRound = _drawService.GetLatestRoundNumberOfGame(game);

            if (_drawService.DidAllPlayersAlreadyDrawInRound(game, currentRound))
                currentRound++;

            if (_drawService.DidPlayerAlreadyDrawInRound(player, currentRound))
                throw new PlayerAlreadyDrawedInRoundException();

            var randomCard = _randomCardService.GetRandomCard(game, currentRound, request.CardType);
            _drawService.AddPlayerDraw(game, player, currentRound, randomCard);

            var response = new DrawResponse
            {
                Card = randomCard,
                GameId = game.Id,
                PlayerId = player.Id,
                Round = currentRound
            };

            if (_drawService.DidAllPlayersAlreadyDrawInRound(game, currentRound))
                response.Winners = _scoreService.GetWinners(game, currentRound);

            return response;
        }
    }
}
