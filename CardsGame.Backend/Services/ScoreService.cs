﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model;
using Services.Abstraction;
using Services.Responses;
using Storage.Repository;

namespace Services
{
    public class ScoreService : IScoreService
    {
        private readonly IRepository<Draw> _drawRepository;

        public ScoreService(IRepository<Draw> drawRepository)
        {
            _drawRepository = drawRepository;
        }

        public IEnumerable<RoundWinner> GetWinners(Game game, int round)
        {
            var scoreGroups = _drawRepository.Query()
                .Where(x => x.GameId == game.Id && x.Round == round)
                .GroupBy(x => x.CardValue)
                .Select(x => new { Score = x.Key, Draws = x })
                .OrderByDescending(x => x.Score);

            if (!scoreGroups.Any())
                throw new ApplicationException("No score groups available for this round game");

            return scoreGroups
                .First()
                .Draws
                .Select(x => new RoundWinner { PlayerId = x.PlayerId, CardId = x.CardId });
        }
    }
}