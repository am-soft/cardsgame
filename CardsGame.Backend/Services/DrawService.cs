﻿using System.Linq;
using Model;
using Services.Abstraction;
using Storage.Repository;

namespace Services
{
    public class DrawService : IDrawService
    {
        private readonly IRepository<Draw> _drawRepository;

        public DrawService(IRepository<Draw> drawRepository)
        {
            _drawRepository = drawRepository;
        }

        public bool DidAllPlayersAlreadyDrawInRound(Game game, int roundNumber)
        {
            return _drawRepository
                       .Query()
                       .Count(x => x.GameId == game.Id && x.Round == roundNumber) == game.PlayersCount;
        }

        public int GetLatestRoundNumberOfGame(Game game)
        {
            var gameDraws = _drawRepository
                .Query()
                .Where(x => x.GameId == game.Id);

            return gameDraws.Any()
                ? gameDraws.Max(x => x.Round)
                : 1;
        }

        public bool DidPlayerAlreadyDrawInRound(Player player, int roundNumber)
        {
            return _drawRepository
                       .Query()
                       .FirstOrDefault(x => x.PlayerId == player.Id && x.Round == roundNumber) != null;
        }

        public Draw AddPlayerDraw(Game game, Player player, int round, Card card)
        {
            var draw = new Draw
            {
                GameId = game.Id,
                PlayerId = player.Id,
                Round = round,
                CardId = card.Id,
                CardValue = card.Attribute.Value
            };
            _drawRepository.Insert(draw);

            return draw;
        }
    }
}