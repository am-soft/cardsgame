﻿using System;
using System.Collections.Generic;
using Model;

namespace Services.Abstraction
{
    public interface IPlayersService
    {
        IEnumerable<Player> AddGamePlayers(Game game, IEnumerable<string> playersNames);
        Player GetGamePlayer(Game game, Guid playerId);
    }
}