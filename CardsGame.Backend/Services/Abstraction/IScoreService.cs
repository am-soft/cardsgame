﻿using System.Collections.Generic;
using Model;
using Services.Responses;

namespace Services.Abstraction
{
    public interface IScoreService
    {
        IEnumerable<RoundWinner> GetWinners(Game game, int round);
    }
}