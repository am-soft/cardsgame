﻿using Model;

namespace Services.Abstraction
{
    public interface IDrawService
    {
        bool DidAllPlayersAlreadyDrawInRound(Game game, int roundNumber);
        int GetLatestRoundNumberOfGame(Game game);
        bool DidPlayerAlreadyDrawInRound(Player player, int roundNumber);
        Draw AddPlayerDraw(Game game, Player player, int round, Card card);
    }
}