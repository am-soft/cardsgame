﻿using Model;

namespace Services.Abstraction
{
    public interface IRandomCardService
    {
        Card GetRandomCard(Game game, int currentRound, CardType cardType);
    }
}