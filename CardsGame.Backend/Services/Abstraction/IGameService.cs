﻿using System;
using Model;
using Services.Requests;
using Services.Responses;

namespace Services.Abstraction
{
    public interface IGameService
    {
        Game StartGame(StartGameRequest request);
        DrawResponse Draw(Guid gameId, DrawRequest request);
    }
}