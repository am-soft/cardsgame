﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model;
using Model.Exceptions;
using Serilog;
using Services.Abstraction;
using Storage.Repository;

namespace Services
{
    public class PlayersService : IPlayersService
    {
        private readonly IRepository<Player> _playerRepository;
        private readonly ILogger _logger;

        public PlayersService(IRepository<Player> playerRepository, ILogger logger)
        {
            _playerRepository = playerRepository;
            _logger = logger;
        }

        public IEnumerable<Player> AddGamePlayers(Game game, IEnumerable<string> playersNames)
        {
            var players = playersNames
                .Select(x => new Player { GameId = game.Id, Name = x })
                .ToList();

            foreach (var player in players)
            {
                _playerRepository.Insert(player);
            }

            _logger.Information($"Added players: {string.Join(", ", players.Select(x => x.Name))} to game ID: {game.Id}");

            return players;
        }

        public Player GetGamePlayer(Game game, Guid playerId)
        {
            var player = _playerRepository.Get(playerId);

            if (player.GameId != game.Id)
                throw new BadRequestException("Player does not belong to this game");

            return player;
        }
    }
}