﻿using System.Collections.Generic;

namespace Model
{
    public class Game : BaseEntity
    {
        public int PlayersCount { get; set; }
        public virtual IEnumerable<Player> Players { get; set; } = new List<Player>();
    }
}