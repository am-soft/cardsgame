﻿using System;

namespace Model
{
    public class Player : BaseEntity
    {
        public string Name { get; set; }
        public Guid GameId { get; set; }
    }
}