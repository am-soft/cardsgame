﻿using System;

namespace Model
{
    public class Card : BaseEntity
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public CardType CardType { get; set; }
        public virtual Attribute Attribute { get; set; }
        public Guid AttributeId { get; set; }
    }
}