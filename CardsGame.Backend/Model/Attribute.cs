﻿namespace Model
{
    public class Attribute : BaseEntity
    {
        public AttributeType Type { get; set; }
        public decimal Value { get; set; }
    }
}