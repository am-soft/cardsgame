﻿using System;

namespace Model
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}