﻿using System;

namespace Model
{
    public class Draw : BaseEntity
    {
        public Guid PlayerId { get; set; }
        public Guid CardId { get; set; }
        public Guid GameId { get; set; }
        public int Round { get; set; }
        public decimal CardValue { get; set; }
    }
}