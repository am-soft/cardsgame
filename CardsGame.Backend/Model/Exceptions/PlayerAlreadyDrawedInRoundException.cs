﻿namespace Model.Exceptions
{
    public class PlayerAlreadyDrawedInRoundException : BadRequestException
    {
        public PlayerAlreadyDrawedInRoundException() : base("This player have already draw a card in this round")
        {
        }
    }
}