﻿using System;

namespace Model.Exceptions
{
    public class EntityNotFoundException : ApplicationException
    {
        public EntityNotFoundException() : base("The requested resource does not exist")
        {
        }
    }
}