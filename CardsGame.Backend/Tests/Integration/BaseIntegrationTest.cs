﻿using System;
using Microsoft.EntityFrameworkCore;
using Model;
using Services;
using Services.Abstraction;
using Storage;
using Storage.Repository;
using Tests.Unit;
using Attribute = Model.Attribute;

namespace Tests.Integration
{
    public class BaseIntegrationTest : BaseUnitTest
    {
        private AppDbContext CreateDb()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>()
               .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
               .Options;

            var dbContext = new AppDbContext(options);

            DatabaseInitializer.InitializeDatabase(dbContext);

            return dbContext;
        }

        protected (AppDbContext dbContext, GameService) CreateGameService()
        {
            var dbContext = CreateDb();
            var gameRepository = new EfRepository<Game>(dbContext);
            var cardRepository = new EfRepository<Card>(dbContext);
            var drawRepository = new EfRepository<Draw>(dbContext);
            var attributesRepository = new EfRepository<Attribute>(dbContext);
            var playerRepository = new EfRepository<Player>(dbContext);
            var randomCardService = new RandomCardService(cardRepository, drawRepository, attributesRepository);
            var playersService = new PlayersService(playerRepository, Logger);
            var drawService = new DrawService(drawRepository);
            var scoreService = new ScoreService(drawRepository);

            return (dbContext,
                new GameService(Logger, gameRepository, randomCardService, playersService, drawService, scoreService));
        }
    }
}