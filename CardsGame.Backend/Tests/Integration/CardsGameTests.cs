﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Services.Requests;
using Storage;
using Xunit;
using CardType = Model.CardType;

namespace Tests.Integration
{
    public class CardsGameTests : BaseIntegrationTest
    {
        [Fact]
        public void StartGame_CreatesGame_And_Adds_Players()
        {
            var (dbContext, service) = CreateGameService();

            var cardType = Faker.PickRandom(CardType.Person, CardType.Starship);
            var startGameRequest = CreateStartGameRequest(dbContext, cardType);

            var response = service.StartGame(startGameRequest);

            var game = dbContext.Games.FirstOrDefault(x => x.Id == response.Id);
            game.Should().NotBeNull();
            game?.PlayersCount.Should().Be(startGameRequest.Players.Count());

            foreach (var player in startGameRequest.Players)
            {
                var dbPlayer = dbContext.Players.FirstOrDefault(x => x.GameId == response.Id && x.Name == player);
                dbPlayer.Should().NotBeNull();
            }
        }

        [Fact]
        public void Draw_Returns_Card_Without_Winners_Before_All_Players_Draw()
        {
            var (dbContext, service) = CreateGameService();

            var cardType = Faker.PickRandom(CardType.Person, CardType.Starship);
            var startGameRequest = CreateStartGameRequest(dbContext, cardType);

            var game = service.StartGame(startGameRequest);
            var players = Faker.Random.Shuffle(dbContext.Players.Where(x => x.GameId == game.Id)).ToList();

            for (var i = 0; i < players.Count - 1; i++)
            {
                var player = players[i];

                var drawRequest = new DrawRequest
                {
                    PlayerId = player.Id,
                    CardType = cardType
                };

                var response = service.Draw(game.Id, drawRequest);

                response.GameId.Should().Be(game.Id);
                response.PlayerId.Should().Be(player.Id);
                response.Round.Should().Be(1);
                response.Winners.Should().BeNull();
            }
        }

        [Fact]
        public void Draw_Returns_Card_With_Winners_When_Last_Player_Draw()
        {
            var (dbContext, service) = CreateGameService();

            var cardType = Faker.PickRandom(CardType.Person, CardType.Starship);
            var startGameRequest = CreateStartGameRequest(dbContext, cardType);

            var game = service.StartGame(startGameRequest);
            var players = Faker.Random.Shuffle(dbContext.Players.Where(x => x.GameId == game.Id)).ToList();

            var rounds = Faker.Random.Int(10, 100);

            for (var round = 1; round < rounds; round++)
            {
                for (var i = 0; i < players.Count; i++)
                {
                    var player = players[i];

                    var drawRequest = new DrawRequest
                    {
                        PlayerId = player.Id,
                        CardType = cardType
                    };

                    var response = service.Draw(game.Id, drawRequest);

                    response.GameId.Should().Be(game.Id);
                    response.PlayerId.Should().Be(player.Id);
                    response.Round.Should().Be(round);

                    if (i == players.Count - 1)
                        response.Winners.Should().NotBeNull();
                    else
                        response.Winners.Should().BeNull();
                }
            }
        }

        private StartGameRequest CreateStartGameRequest(AppDbContext dbContext, CardType cardType)
        {
            var playersCount = Faker.Random.Int(2, dbContext.Cards.Count(x => x.CardType == cardType));
            var playersNames = new List<string>();
            for (var i = 0; i < playersCount; i++)
            {
                playersNames.Add(Faker.Name.FirstName());
            }

            var startGameRequest = new StartGameRequest
            {
                Players = playersNames
            };

            return startGameRequest;
        }

    }
}