﻿using System.Collections.Generic;
using Bogus;
using FizzWare.NBuilder;
using Model;
using Moq;
using Serilog;
using Services.Abstraction;
using Storage.Repository;

namespace Tests.Unit
{
    public class BaseUnitTest
    {
        protected readonly Faker Faker = new Faker();
        protected readonly Builder Builder = new Builder();
        protected readonly ILogger Logger = new Mock<ILogger>().Object;
        protected Mock<T> Mock<T>() where T : class
        {
            return new Mock<T>();
        }

        protected (Game game, IList<Player> players) CreateGameWithPlayers()
        {
            var mocks = (Mock<IRepository<Game>>(), Mock<IPlayersService>());
            return CreateGameWithPlayers(mocks);
        }

        protected (Game game, IList<Player> players) CreateGameWithPlayers((Mock<IRepository<Game>> gameRepository, Mock<IPlayersService> playersService) mocks)
        {
            var gameId = Faker.Random.Guid();

            var players = Builder.CreateListOfSize<Player>(2)
                .All()
                .With(x => x.GameId, gameId)
                .Build();

            var game = Builder.CreateNew<Game>()
                .With(x => x.Id, gameId)
                .With(x => x.Players, players)
                .With(x => x.PlayersCount, players.Count)
                .Build();

            mocks.gameRepository.Setup(x => x.Get(game.Id))
                .Returns(game);

            mocks.playersService.Setup(x => x.GetGamePlayer(game, players[0].Id))
                .Returns(players[0]);

            mocks.playersService.Setup(x => x.GetGamePlayer(game, players[1].Id))
                .Returns(players[1]);

            return (game, players);
        }
    }
}