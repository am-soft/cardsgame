﻿using System;
using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Model;
using Moq;
using Services;
using Storage.Repository;
using Xunit;
using Attribute = Model.Attribute;

namespace Tests.Unit.Services.RandomCardServiceTests
{
    public class RandomCardServiceTests : BaseUnitTest
    {
        [Fact]
        public void GetRandomCard_Should_Throw_If_Cards_Empty()
        {
            var gameWithPlayers = CreateGameWithPlayers();
            var cardsRepository = Mock<IRepository<Card>>();
            var drawRepository = Mock<IRepository<Draw>>();
            var attributesRepository = Mock<IRepository<Attribute>>();

            var cardType = Faker.PickRandom(CardType.Person, CardType.Starship);
            var round = Faker.Random.Int(1);

            var service = new RandomCardService(cardsRepository.Object, drawRepository.Object, attributesRepository.Object);

            Assert.Throws<InvalidOperationException>(() =>
                service.GetRandomCard(gameWithPlayers.game, round, cardType));
        }

        [Fact]
        public void GetRandomCard_Should_Return_Card_Not_Drawn_In_Round()
        {
            var gameWithPlayers = CreateGameWithPlayers();
            var cardsRepository = Mock<IRepository<Card>>();
            var drawRepository = Mock<IRepository<Draw>>();
            var attributesRepository = Mock<IRepository<Attribute>>();

            var cards = new List<Card>();
            var attributes = new List<Attribute>();
            var cardType = Faker.PickRandom(CardType.Person, CardType.Starship);

            for (var i = 0; i < Faker.Random.Int(1, 100); i++)
            {
                var attribute = Builder.CreateNew<Attribute>().With(x => x.Id, Faker.Random.Guid())
                    .Build();
                attributes.Add(attribute);
                cards.Add(Builder.CreateNew<Card>()
                    .With(x => x.Id, Faker.Random.Guid())
                    .With(x => x.CardType, cardType)
                    .With(x => x.Attribute, attribute)
                    .With(x => x.AttributeId, attribute.Id).Build()
                );
            }

            cardsRepository.Setup(x => x.Query())
                    .Returns(cards.AsQueryable());
            attributesRepository.Setup(x => x.Query())
                    .Returns(attributes.AsQueryable());

            var round = Faker.Random.Int(1);
            var drawCard = Faker.PickRandom(cards);

            drawRepository.Setup(x => x.Query())
                    .Returns(new[]
                        {
                        Builder.CreateNew<Draw>()
                            .With(x => x.CardId, drawCard.Id)
                            .With(x => x.GameId, gameWithPlayers.game.Id)
                            .With(x => x.CardValue, drawCard.Attribute.Value)
                            .With(x => x.PlayerId, gameWithPlayers.players[0].Id)
                            .With(x => x.Round, round)
                            .Build()
                        }
                        .AsQueryable());

            var service = new RandomCardService(cardsRepository.Object, drawRepository.Object, attributesRepository.Object);

            var card = service.GetRandomCard(gameWithPlayers.game, round, cardType);
            card.Should().NotBeNull();
            card.Should().NotBeEquivalentTo(drawCard);
        }
    }
}