﻿using System;
using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using FluentAssertions;
using Model;
using Services;
using Storage.Repository;
using Xunit;

namespace Tests.Unit.Services.ScoreServiceTests
{
    public class ScoreServiceTests : BaseUnitTest
    {
        [Fact]
        public void GetWinners_Should_Throw_For_Empty_Draws()
        {
            var drawRepository = Mock<IRepository<Draw>>();
            var service = new ScoreService(drawRepository.Object);

            drawRepository.Setup(x => x.Query())
                .Returns(new List<Draw>().AsQueryable());

            var gameWithPlayers = CreateGameWithPlayers();
            Assert.Throws<ApplicationException>(() => service.GetWinners(gameWithPlayers.game, Faker.Random.Number(1)));
        }

        [Fact]
        public void GetWinners_Should_Return_Winner_For_Single_Draw()
        {
            var drawRepository = Mock<IRepository<Draw>>();
            var service = new ScoreService(drawRepository.Object);
            var gameWithPlayers = CreateGameWithPlayers();
            var round = Faker.Random.Int(1);

            var draws = Builder.CreateListOfSize<Draw>(1)
                .All()
                .With(x => x.GameId, gameWithPlayers.game.Id)
                .With(x => x.Round, round)
                .Build();

            drawRepository.Setup(x => x.Query())
                .Returns(draws.AsQueryable());

            var winner = draws.OrderByDescending(x => x.CardValue).First();

            var result = service.GetWinners(gameWithPlayers.game, round).ToList();

            result.First().PlayerId.Should().Be(winner.PlayerId);
            result.First().CardId.Should().Be(winner.CardId);
        }

        [Fact]
        public void GetWinners_Should_Return_Winner_For_Multiple_Rounds()
        {
            var drawRepository = Mock<IRepository<Draw>>();
            var service = new ScoreService(drawRepository.Object);
            var gameWithPlayers = CreateGameWithPlayers();

            var draws = new List<Draw>();
            var roundNumber = 1;
            for (var i = 0; i < Faker.Random.Number(5, 999); i++)
            {
                var roundDraws = Builder.CreateListOfSize<Draw>(Faker.Random.Int(2, 100))
                    .All()
                    .With(x => x.GameId, gameWithPlayers.game.Id)
                    .With(x => x.Round, roundNumber)
                    .Build();

                draws.AddRange(roundDraws);
                roundNumber++;
            }

            drawRepository.Setup(x => x.Query())
                .Returns(draws.AsQueryable());

            for (var i = 1; i < roundNumber; i++)
            {
                var round = i;
                var winner = draws
                    .Where(x => x.Round == round)
                    .OrderByDescending(x => x.CardValue)
                    .First();

                var result = service.GetWinners(gameWithPlayers.game, round).ToList();

                result.First().CardId.Should().Be(winner.CardId);
                result.First().PlayerId.Should().Be(winner.PlayerId);
            }
        }

        [Fact]
        public void GetWinners_Should_Return_Multiple_Winners()
        {
            var drawRepository = Mock<IRepository<Draw>>();
            var service = new ScoreService(drawRepository.Object);
            var gameWithPlayers = CreateGameWithPlayers();
            var round = Faker.Random.Int(1);

            var numberOrWinners = Faker.Random.Int(3, 100);
            var winningValue = Faker.Random.Decimal(1, 1000);

            var draws = Builder.CreateListOfSize<Draw>(numberOrWinners)
                .All()
                .With(x => x.GameId, gameWithPlayers.game.Id)
                .With(x => x.Round, round)
                .With(x => x.CardValue, winningValue)
                .Build();

            drawRepository.Setup(x => x.Query())
                .Returns(draws.AsQueryable());

            var result = service.GetWinners(gameWithPlayers.game, round).ToList();

            result.Count.Should().Be(numberOrWinners);
            foreach (var draw in draws)
            {
                result.Select(x => x.PlayerId).Should().Contain(draw.PlayerId);
                result.Select(x => x.CardId).Should().Contain(draw.CardId);
            }
        }
    }
}