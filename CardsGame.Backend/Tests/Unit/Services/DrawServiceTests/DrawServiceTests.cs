﻿using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using FluentAssertions;
using Model;
using Moq;
using Services;
using Storage.Repository;
using Xunit;

namespace Tests.Unit.Services.DrawServiceTests
{
    public class DrawServiceTests : BaseUnitTest
    {
        [Fact]
        public void DidAllPlayersAlreadyDrawInRound_Should_Return_False_For_NoDraws()
        {
            var drawRepository = Mock<IRepository<Draw>>();

            var game = Builder.CreateNew<Game>()
                .With(x => x.PlayersCount, Faker.Random.Int(2))
                .Build();

            drawRepository.Setup(x => x.Query())
                .Returns(new List<Draw>().AsQueryable());

            var service = new DrawService(drawRepository.Object);

            var result = service.DidAllPlayersAlreadyDrawInRound(game, Faker.Random.Int());
            result.Should().BeFalse();
        }

        [Fact]
        public void DidAllPlayersAlreadyDrawInRound_Should_Return_False_For_One_Of_All_Players_Draw()
        {
            var drawRepository = Mock<IRepository<Draw>>();

            var game = Builder.CreateNew<Game>()
                .With(x => x.PlayersCount, Faker.Random.Int(2, 999))
                .Build();

            var roundNumber = Faker.Random.Int(1);

            var draw = Builder.CreateNew<Draw>()
                .With(x => x.Round, roundNumber)
                .With(x => x.GameId, game.Id)
                .Build();

            drawRepository.Setup(x => x.Query())
                .Returns(new[] { draw }.AsQueryable());

            var service = new DrawService(drawRepository.Object);

            var result = service.DidAllPlayersAlreadyDrawInRound(game, roundNumber);
            result.Should().BeFalse();
        }

        [Fact]
        public void DidAllPlayersAlreadyDrawInRound_Should_Return_True_For_All_Players_Draw()
        {
            var drawRepository = Mock<IRepository<Draw>>();

            var game = Builder.CreateNew<Game>()
                .With(x => x.PlayersCount, Faker.Random.Int(2, 999))
                .Build();

            var roundNumber = Faker.Random.Int(1);

            var draws = new List<Draw>();

            for (var i = 0; i < game.PlayersCount; i++)
            {
                var draw = Builder.CreateNew<Draw>()
                    .With(x => x.Round, roundNumber)
                    .With(x => x.GameId, game.Id)
                    .Build();

                draws.Add(draw);
            }

            drawRepository.Setup(x => x.Query())
                .Returns(draws.AsQueryable());

            var service = new DrawService(drawRepository.Object);

            var result = service.DidAllPlayersAlreadyDrawInRound(game, roundNumber);
            result.Should().BeTrue();
        }

        [Fact]
        public void GetLatestRoundNumberOfGame_Should_Return_1_For_Empty_Draws()
        {
            var drawRepository = Mock<IRepository<Draw>>();

            var game = Builder.CreateNew<Game>()
                .With(x => x.PlayersCount, Faker.Random.Int(2))
                .Build();

            drawRepository.Setup(x => x.Query())
                .Returns(new List<Draw>().AsQueryable());

            var service = new DrawService(drawRepository.Object);

            var result = service.GetLatestRoundNumberOfGame(game);
            result.Should().Be(1);
        }

        [Fact]
        public void GetLatestRoundNumberOfGame_Should_Return_Greatest_Round_Number()
        {
            var drawRepository = Mock<IRepository<Draw>>();

            var game = Builder.CreateNew<Game>()
                .With(x => x.PlayersCount, Faker.Random.Int(2))
                .Build();

            var draws = Builder.CreateListOfSize<Draw>(Faker.Random.Int(100, 1000))
                .All()
                .With(x => x.GameId, game.Id)
                .Build();

            drawRepository.Setup(x => x.Query())
                .Returns(draws.AsQueryable());

            var greatestRoundNumber = draws.OrderByDescending(x => x.Round).First().Round;

            var service = new DrawService(drawRepository.Object);

            var result = service.GetLatestRoundNumberOfGame(game);
            result.Should().Be(greatestRoundNumber);
        }

        [Fact]
        public void DidPlayerAlreadyDrawInRound_Should_Return_False_For_Empty_Draws()
        {
            var drawRepository = Mock<IRepository<Draw>>();

            var gameWithPlayers = CreateGameWithPlayers();

            drawRepository.Setup(x => x.Query())
                .Returns(new List<Draw>().AsQueryable());

            var service = new DrawService(drawRepository.Object);

            var result = service.DidPlayerAlreadyDrawInRound(gameWithPlayers.players[0], Faker.Random.Int(1));
            result.Should().BeFalse();
        }

        [Fact]
        public void DidPlayerAlreadyDrawInRound_Should_Return_True_When_Player_Draw()
        {
            var drawRepository = Mock<IRepository<Draw>>();

            var gameWithPlayers = CreateGameWithPlayers();
            var playerWhoDraw = Faker.PickRandom(gameWithPlayers.players);
            var roundNumber = Faker.Random.Int(1);

            var draws = new List<Draw>
            {
                Builder.CreateNew<Draw>()
                    .With(x => x.PlayerId, playerWhoDraw.Id)
                    .With(x => x.Round, roundNumber)
                    .Build()
            };

            drawRepository.Setup(x => x.Query())
                .Returns(draws.AsQueryable());

            var service = new DrawService(drawRepository.Object);

            var result = service.DidPlayerAlreadyDrawInRound(playerWhoDraw, roundNumber);
            result.Should().BeTrue();

            result = service.DidPlayerAlreadyDrawInRound(gameWithPlayers.players.First(x => x.Id != playerWhoDraw.Id), roundNumber);
            result.Should().BeFalse();
        }

        [Fact]
        public void AddPlayerDraw_Should_Insert_Draw()
        {
            var drawRepository = Mock<IRepository<Draw>>();

            var gameWithPlayers = CreateGameWithPlayers();
            var attribute = Builder.CreateNew<Attribute>().Build();
            var card = Builder.CreateNew<Card>()
                .With(x => x.Attribute, attribute)
                .Build();
            var roundNumber = Faker.Random.Int(1);

            var service = new DrawService(drawRepository.Object);

            foreach (var player in gameWithPlayers.players)
            {
                var result = service.AddPlayerDraw(gameWithPlayers.game, player, roundNumber, card);
                result.CardId.Should().Be(card.Id);
                result.CardValue.Should().Be(card.Attribute.Value);
                result.GameId.Should().Be(gameWithPlayers.game.Id);
                result.PlayerId.Should().Be(player.Id);
                result.Round.Should().Be(roundNumber);
                drawRepository.Verify(x => x.Insert(It.Is<Draw>(draw => draw.GameId == gameWithPlayers.game.Id && draw.PlayerId == player.Id)), Times.Once);
            }
        }
    }
}