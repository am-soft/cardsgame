﻿using Model;
using Moq;
using Services;
using Services.Abstraction;
using Storage.Repository;

namespace Tests.Unit.Services.GameServiceTests
{
    public class GameServiceBaseTest : BaseUnitTest
    {

        protected (Mock<IRepository<Game>> gameRepository, Mock<IRandomCardService> randomCardService, Mock<IPlayersService> playersService, Mock<IDrawService> drawService, Mock<IScoreService> scoreService) CreateMocks()
        {
            var gameRepository = new Mock<IRepository<Game>>();
            var randomCardService = new Mock<IRandomCardService>();
            var playersService = new Mock<IPlayersService>();
            var drawService = new Mock<IDrawService>();
            var scoreService = new Mock<IScoreService>();

            return (gameRepository, randomCardService, playersService, drawService, scoreService);
        }

        protected IGameService CreateGameService((Mock<IRepository<Game>> gameRepository, Mock<IRandomCardService> randomCardService, Mock<IPlayersService> playersService, Mock<IDrawService> drawService, Mock<IScoreService> scoreService) mocks)
        {
            return new GameService(Logger, mocks.gameRepository.Object, mocks.randomCardService.Object, mocks.playersService.Object, mocks.drawService.Object, mocks.scoreService.Object);
        }
    }
}