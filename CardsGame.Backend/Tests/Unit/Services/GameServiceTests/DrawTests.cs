﻿using System;
using System.Linq;
using FizzWare.NBuilder;
using FluentAssertions;
using Model;
using Model.Exceptions;
using Moq;
using Services.Requests;
using Services.Responses;
using Xunit;
using Attribute = Model.Attribute;

namespace Tests.Unit.Services.GameServiceTests
{
    public class DrawTests : GameServiceBaseTest
    {
        [Fact]
        public void Draw_Should_Throw_When_Null_Request()
        {
            var service = CreateGameService(CreateMocks());
            Assert.Throws<BadRequestException>(() => service.Draw(Guid.Empty, null));
        }

        [Fact]
        public void Draw_Should_Throw_When_Player_Already_Draw()
        {
            var mocks = CreateMocks();
            var service = CreateGameService(mocks);
            var gameWithPlayers = CreateGameWithPlayers((mocks.gameRepository, mocks.playersService));

            var randomPlayer = Faker.PickRandom(gameWithPlayers.players);

            var drawRequest = new DrawRequest
            {
                CardType = Faker.PickRandom(CardType.Person, CardType.Starship),
                PlayerId = randomPlayer.Id
            };

            mocks.drawService.Setup(x => x.DidPlayerAlreadyDrawInRound(It.IsAny<Player>(), It.IsAny<int>()))
                .Returns(true);

            Assert.Throws<PlayerAlreadyDrawedInRoundException>(() => service.Draw(gameWithPlayers.game.Id, drawRequest));
        }

        [Fact]
        public void Draw_Should_Increase_RoundNumber_When_AllPlayersAlreadyDraw()
        {
            var mocks = CreateMocks();
            var service = CreateGameService(mocks);
            var gameWithPlayers = CreateGameWithPlayers((mocks.gameRepository, mocks.playersService));

            var roundNumber = Faker.Random.Int(1);

            mocks.drawService.Setup(x => x.DidAllPlayersAlreadyDrawInRound(gameWithPlayers.game, roundNumber))
                .Returns(true);

            mocks.drawService.Setup(x => x.GetLatestRoundNumberOfGame(It.IsAny<Game>()))
                .Returns(roundNumber);

            var request = Builder.CreateNew<DrawRequest>()
                .With(x => x.PlayerId, gameWithPlayers.players[0].Id)
                .Build();

            var response = service.Draw(gameWithPlayers.game.Id, request);

            response.Round.Should().Be(roundNumber + 1);
        }

        [Fact]
        public void Draw_Should_Return_Valid_Response_WithoutWinners()
        {
            var mocks = CreateMocks();
            var service = CreateGameService(mocks);
            var gameWithPlayers = CreateGameWithPlayers((mocks.gameRepository, mocks.playersService));
            var cardType = Faker.PickRandom(CardType.Person, CardType.Starship);
            var attribute = Builder.CreateNew<Attribute>().Build();
            var card = Builder.CreateNew<Card>()
                .With(x => x.Attribute, attribute)
                .Build();
            var round = 1;

            mocks.drawService.Setup(x => x.GetLatestRoundNumberOfGame(It.IsAny<Game>()))
                .Returns(round);

            mocks.randomCardService.Setup(x => x.GetRandomCard(gameWithPlayers.game, round, cardType))
                .Returns(card);

            var drawRequest = Builder.CreateNew<DrawRequest>()
                .With(x => x.CardType, cardType)
                .With(x => x.PlayerId, Faker.PickRandom(gameWithPlayers.players).Id)
                .Build();

            var response = service.Draw(gameWithPlayers.game.Id, drawRequest);

            response.Round.Should().Be(round);
            response.PlayerId.Should().Be(drawRequest.PlayerId);
            response.Card.Should().Be(card);
            response.Winners.Should().BeNull();
            response.GameId.Should().Be(gameWithPlayers.game.Id);
        }

        [Fact]
        public void Draw_Should_Return_Valid_Response_WithWinners()
        {
            var mocks = CreateMocks();
            var service = CreateGameService(mocks);
            var gameWithPlayers = CreateGameWithPlayers((mocks.gameRepository, mocks.playersService));
            var cardType = Faker.PickRandom(CardType.Person, CardType.Starship);
            var attribute = Builder.CreateNew<Attribute>().Build();
            var card = Builder.CreateNew<Card>()
                .With(x => x.Attribute, attribute)
                .Build();
            var round = 1;

            var winningPlayer = Faker.PickRandom(gameWithPlayers.players);
            var winningCardId = Faker.Random.Guid();
            mocks.scoreService.Setup(x => x.GetWinners(gameWithPlayers.game, It.IsAny<int>()))
                .Returns(new[] { new RoundWinner { PlayerId = winningPlayer.Id, CardId = winningCardId } });

            mocks.drawService.Setup(x => x.DidAllPlayersAlreadyDrawInRound(It.IsAny<Game>(), It.IsAny<int>()))
                .Returns(true);
            mocks.drawService.Setup(x => x.GetLatestRoundNumberOfGame(It.IsAny<Game>()))
                .Returns(round);
            mocks.randomCardService.Setup(x => x.GetRandomCard(It.IsAny<Game>(), It.IsAny<int>(), It.IsAny<CardType>()))
                .Returns(card);

            var drawRequest = new DrawRequest
            {
                PlayerId = Faker.PickRandom(gameWithPlayers.players).Id,
                CardType = cardType
            };

            var response = service.Draw(gameWithPlayers.game.Id, drawRequest);

            response.Round.Should().Be(round + 1);
            response.PlayerId.Should().Be(drawRequest.PlayerId);
            response.Card.Should().Be(card);
            response.GameId.Should().Be(gameWithPlayers.game.Id);
            response.Winners.Should().NotBeNull();
            response.Winners.First().PlayerId.Should().Be(winningPlayer.Id);
            response.Winners.First().CardId.Should().Be(winningCardId);
        }
    }
}
