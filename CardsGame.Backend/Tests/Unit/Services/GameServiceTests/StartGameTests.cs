﻿using System;
using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using FluentAssertions;
using Model;
using Model.Exceptions;
using Moq;
using Services.Requests;
using Xunit;

namespace Tests.Unit.Services.GameServiceTests
{
    public class StartGameTests : GameServiceBaseTest
    {
        [Fact]
        public void StartGame_Should_Insert_Game_GetRandomCard_And_Add_Players()
        {
            var request = Builder.CreateNew<StartGameRequest>()
                .With(x => x.Players, new[] { 1, 2 }.Select(x => Faker.Name.FirstName()))
                .Build();

            var mocks = CreateMocks();
            var service = CreateGameService(mocks);
            var game = service.StartGame(request);

            mocks.gameRepository.Verify(gameRepository => gameRepository.Insert(It.IsAny<Game>()), Times.Once);
            mocks.gameRepository.VerifyNoOtherCalls();

            mocks.playersService.Verify(playersService => playersService.AddGamePlayers(game, request.Players), Times.Once);
            mocks.playersService.VerifyNoOtherCalls();

            game.PlayersCount.Should().Be(request.Players.Count());
        }

        [Fact]
        public void StartGame_Should_Throw_When_Null_Request()
        {
            var service = CreateGameService(CreateMocks());
            Assert.Throws<BadRequestException>(() => service.StartGame(null));
        }

        [Fact]
        public void StartGame_Should_Throw_When_Null_Players()
        {
            var service = CreateGameService(CreateMocks());
            var request = new StartGameRequest();
            Assert.Throws<BadRequestException>(() => service.StartGame(request));
        }

        [Fact]
        public void StartGame_Should_Throw_When_Empty_Players()
        {
            var service = CreateGameService(CreateMocks());
            var request = new StartGameRequest { Players = new List<string>() };
            Assert.Throws<BadRequestException>(() => service.StartGame(request));
        }

        [Fact]
        public void Draw_Should_Throw_When_Null_Request()
        {
            var service = CreateGameService(CreateMocks());
            Assert.Throws<BadRequestException>(() => service.Draw(Guid.Empty, null));
        }
    }
}
