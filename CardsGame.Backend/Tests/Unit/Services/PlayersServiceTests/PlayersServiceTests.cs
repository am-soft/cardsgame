﻿using System;
using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using FluentAssertions;
using Model;
using Model.Exceptions;
using Moq;
using Services;
using Storage.Repository;
using Xunit;

namespace Tests.Unit.Services.PlayersServiceTests
{
    public class PlayersServiceTests : BaseUnitTest
    {
        [Fact]
        public void AddGamePlayers_Should_Insert_Players()
        {
            var playersRepository = Mock<IRepository<Player>>();
            var gameWithPlayers = CreateGameWithPlayers();

            var service = new PlayersService(playersRepository.Object, Logger);

            var playersNames = new List<string>();
            for (var i = 0; i < Faker.Random.Number(5, 999); i++)
            {
                playersNames.Add(Faker.Name.FirstName());
            }

            var result = service.AddGamePlayers(gameWithPlayers.game, playersNames).ToList();

            result.Count.Should().Be(playersNames.Count);
            result.All(x => x.GameId == gameWithPlayers.game.Id).Should().BeTrue();

            foreach (var playerName in playersNames)
            {
                result.Select(x => x.Name).Should().Contain(playerName);
            }

            playersRepository.Verify(x => x.Insert(It.IsAny<Player>()), Times.Exactly(playersNames.Count));
        }

        [Fact]
        public void GetGamePlayer_Should_Throw_For_Player_Not_Assigned_To_Requested_Game()
        {
            var playersRepository = Mock<IRepository<Player>>();

            var games = Builder.CreateListOfSize<Game>(2)
                .Build();

            var players = Builder.CreateListOfSize<Player>(20)
                .Build();

            for (var i = 0; i < 10; i++)
            {
                players[i].GameId = games[0].Id;
            }

            for (var i = 10; i < 20; i++)
            {
                players[i].GameId = games[1].Id;
            }

            var players1 = players.Take(10).ToList();
            var players2 = players.Skip(10).Take(10).ToList();

            foreach (var player in players1.Concat(players2))
            {
                playersRepository.Setup(x => x.Get(It.Is<Guid>(y => y == player.Id)))
                    .Returns(player);
            }

            var service = new PlayersService(playersRepository.Object, Logger);

            foreach (var player in players1)
            {
                Assert.Throws<BadRequestException>(() => service.GetGamePlayer(games[1], player.Id));
            }

            foreach (var player in players2)
            {
                Assert.Throws<BadRequestException>(() => service.GetGamePlayer(games[0], player.Id));
            }
        }

        [Fact]
        public void GetGamePlayer_Should_Return_Valid_Player()
        {
            var gameWithPlayers = CreateGameWithPlayers();
            var playersRepository = Mock<IRepository<Player>>();

            foreach (var player in gameWithPlayers.players)
            {
                playersRepository.Setup(x => x.Get(It.Is<Guid>(guid => guid == player.Id)))
                    .Returns(player);
            }

            var service = new PlayersService(playersRepository.Object, Logger);

            foreach (var player in gameWithPlayers.players)
            {
                var result = service.GetGamePlayer(gameWithPlayers.game, player.Id);
                result.GameId.Should().Be(gameWithPlayers.game.Id);
                result.Name.Should().Be(player.Name);
                result.Id.Should().Be(player.Id);
            }
        }
    }
}