﻿using System;
using Api.Controllers;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Model.Exceptions;
using Xunit;

namespace Tests.Unit.Controllers
{
    public class BaseControllerTests : BaseController
    {
        [Fact]
        public void Should_Return_Ok_For_No_Exception()
        {
            var result = ProcessRequest(() => "Success");
            result.Should().BeOfType<OkObjectResult>();
        }

        [Fact]
        public void Should_Return_NotFound_For_EntityNotFoundException()
        {
            var result = ProcessRequest(() => throw new EntityNotFoundException());
            result.Should().BeOfType<NotFoundObjectResult>();
        }

        [Fact]
        public void Should_Return_Unauthorized_For_UnauthorizedAccessException()
        {
            var result = ProcessRequest(() => throw new UnauthorizedAccessException());
            result.Should().BeOfType<UnauthorizedObjectResult>();
        }

        [Fact]
        public void Should_Return_BadRequest_For_BadRequestException()
        {
            var result = ProcessRequest(() => throw new BadRequestException("message"));
            result.Should().BeOfType<BadRequestObjectResult>();
        }

        [Fact]
        public void Should_Return_Status500_For_UnhandledException()
        {
            var result = ProcessRequest(() => throw new Exception());
            result.Should().BeOfType<ObjectResult>();
        }
    }
}