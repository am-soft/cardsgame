﻿using System.Linq;
using Xunit;
using Api.Controllers;
using Model;
using Services.Abstraction;
using Services.Requests;

namespace Tests.Unit.Controllers
{
    public class GameControllerTests : BaseUnitTest
    {
        [Fact]
        public void NewGame_Action_Should_Call_GameService()
        {
            var mockGameService = Mock<IGameService>();
            var controller = new GameController(mockGameService.Object);

            var request = new StartGameRequest { Players = new[] { 1, 2 }.Select(x => Faker.Name.FirstName()) };

            controller.NewGame(request);
            controller.Dispose();

            mockGameService.Verify(service => service.StartGame(request));
            mockGameService.VerifyNoOtherCalls();
        }

        [Fact]
        public void Draw_Action_Should_Call_GameService()
        {
            var mockGameService = Mock<IGameService>();
            var controller = new GameController(mockGameService.Object);

            var request = new DrawRequest
            {
                CardType = Faker.PickRandom<CardType>(new[] { CardType.Person, CardType.Starship }),
                PlayerId = Faker.Random.Guid()
            };
            var gameId = Faker.Random.Guid();

            controller.Draw(gameId, request);
            controller.Dispose();

            mockGameService.Verify(service => service.Draw(gameId, request));
            mockGameService.VerifyNoOtherCalls();
        }
    }
}