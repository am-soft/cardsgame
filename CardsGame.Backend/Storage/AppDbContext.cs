﻿using Microsoft.EntityFrameworkCore;
using Model;

namespace Storage
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<Game> Games { get; set; }

        public DbSet<Card> Cards { get; set; }

        public DbSet<Attribute> Attributes { get; set; }

        public DbSet<Player> Players { get; set; }

        public DbSet<Draw> Draws { get; set; }
    }
}
