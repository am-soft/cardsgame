﻿using Model;

namespace Storage
{
    public static class DatabaseInitializer
    {
        public static void InitializeDatabase(AppDbContext appDbContext)
        {
            appDbContext.Cards.AddRange(
                new Card
                {
                    Attribute = new Attribute { Type = AttributeType.Crew, Value = 3 },
                    Image = "orzel-7.jpg",
                    Name = "ORZEŁ-7",
                    CardType = CardType.Starship
                },
                new Card
                {
                    Attribute = new Attribute { Type = AttributeType.Crew, Value = 7 },
                    Image = "discovery.jpg",
                    Name = "Discovery",
                    CardType = CardType.Starship
                },
                new Card
                {
                    Attribute = new Attribute { Type = AttributeType.Crew, Value = 3 },
                    Image = "apollo-8.jpg",
                    Name = "Apollo 8",
                    CardType = CardType.Starship
                },
                new Card
                {
                    Attribute = new Attribute { Type = AttributeType.Crew, Value = 5 },
                    Image = "atlantis.jpg",
                    Name = "Atlantis",
                    CardType = CardType.Starship
                },
                new Card
                {
                    Attribute = new Attribute { Type = AttributeType.Crew, Value = 6 },
                    Image = "buran.jpg",
                    Name = "Buran",
                    CardType = CardType.Starship
                },
                new Card
                {
                    Attribute = new Attribute { Type = AttributeType.Weight, Value = 74 },
                    Image = "gates.jpeg",
                    Name = "Bill Gates",
                    CardType = CardType.Person
                },
                new Card
                {
                    Attribute = new Attribute { Type = AttributeType.Weight, Value = 100 },
                    Image = "trump.jpg",
                    Name = "Donald Trump",
                    CardType = CardType.Person
                },
                new Card
                {
                    Attribute = new Attribute { Type = AttributeType.Weight, Value = 82 },
                    Image = "musk.png",
                    Name = "Elon Musk",
                    CardType = CardType.Person
                },
                new Card
                {
                    Attribute = new Attribute { Type = AttributeType.Weight, Value = 75 },
                    Image = "dicaprio.jpg",
                    Name = "Leonardo Dicaprio",
                    CardType = CardType.Person
                }
            );

            appDbContext.SaveChanges();
        }
    }
}