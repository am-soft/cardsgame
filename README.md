# Cards Game - recruitment task

This project was created by me in order to solve recruitment task for Full-Stack Developer position.

## Solution structure

### CardsGame.Backend

API written in .NET Core.

- `Api` - .NET Core Web Application which provides endpoints for remote clients.
- `Model` - Application domain classes library.
- `Services` - Contains business logic grouped in services separated by responsibility.
- `Storage` - Database related operations. This project use In-Memory Database.
- `Tests` - Unit & Integration tests of Backend.

### CardsGame.Web

Angular Web Application which is the user interface.

- `src/app/game/components` - Components of Cards Game.
- `src/app/game/models` - Model classes used to exchange data with Backend.
- `src/app/game/services` - Services used for Frontend controll and to communicate with Backend.

## Requirements to run

- Visual Studio 2019
- NodeJS v10.0 or newer

## How to run

1. Open `CardsGame.Backend/CardsGame.Backend.sln` solution file with Visual Studio.
2. Restore solution NuGet packages and build solution.
3. Run `Api` project.
4. Backend application should be available on `https://localhost:44355/swagger/`.
5. `cmd` into `CardsGame.Web` folder.
6. Run `npm i` to install NPM packages.
7. Run `ng serve` to run application
8. Frontend application should be available on address http://localhost:4200/
