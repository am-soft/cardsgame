import { TestBed } from '@angular/core/testing';

import { ApiService } from './api.service';
import { GameModule } from '../game.module';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('ApiService', () => {
  let httpTestingController: HttpTestingController;
  let service: ApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [ApiService], imports: [HttpClientTestingModule] });

    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(ApiService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call POST to API with valid URL', () => {
    const service: ApiService = TestBed.get(ApiService);
    const path = 'some/test/path';
    const someBody = { some: 'body' };

    service.post(path, {}).subscribe(body => expect(body).toEqual(someBody));

    const requestMade = httpTestingController.expectOne(ApiService.BaseUrl + path);
    expect(requestMade.request.method).toBe('POST');

    requestMade.flush(someBody);
  });
});
