import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public static BaseUrl = 'https://localhost:44355/';

  constructor(private http: HttpClient) {}

  public post<T>(path: string, body: {}): Observable<T> {
    const obs = this.http.post<T>(ApiService.BaseUrl + path, body).pipe(share());
    return obs;
  }
}
