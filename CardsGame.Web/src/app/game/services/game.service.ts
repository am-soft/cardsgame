import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import StartGameRequest from '../models/startGameRequest';
import CardType from '../models/cardType';
import Game from '../models/game';
import Player from '../models/player';
import DrawRequest from '../models/drawRequest';
import DrawResponse from '../models/drawResponse';
import Card from '../models/card';
import RoundWinner from '../models/roundWinner';
import PlayerType from '../models/playerType';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private apiService: ApiService;
  
  game: Game;
  scores: { [playerId: string]: number };
  cardType: CardType = null;
  playerCard: Card;
  enemyCard: Card;
  winners: RoundWinner[];

  constructor(http: HttpClient) {
    this.scores = {};
    this.apiService = new ApiService(http);
  }

  startGame(cardType: CardType) {
    if (this.game) throw new Error('Trying to start game while already playing');

    this.cardType = cardType;

    const request: StartGameRequest = {
      players: ['You', 'Enemy']
    };

    const obs = this.apiService.post<Game>('game', request);
    obs.subscribe(game => {
      this.game = game;
    });

    return obs;
  }

  drawPlayer() {
    return this.draw(this.game.players[0], PlayerType.Player);
  }

  drawEnemy() {
    return this.draw(this.game.players[1], PlayerType.Enemy);
  }

  nextRound() {
    this.playerCard = null;
    this.enemyCard = null;
    this.winners = null;
  }

  private draw(player: Player, playerType: PlayerType) {
    const request: DrawRequest = {
      cardType: this.cardType,
      playerId: player.id
    };

    const obs = this.apiService.post<DrawResponse>(`game/${this.game.id}/draw`, request);
    obs.subscribe(drawResponse => this.processDrawResponse(playerType, drawResponse));
    return obs;
  }

  private processDrawResponse(playerType: PlayerType, drawResponse: DrawResponse) {
    if (playerType === PlayerType.Player) {
      this.playerCard = drawResponse.card;
    } else if (playerType === PlayerType.Enemy) {
      this.enemyCard = drawResponse.card;
    }

    if (drawResponse.winners) {
      this.updateScores(drawResponse);
      this.decorateWinners(drawResponse);
    }
  }

  private updateScores(drawResponse: DrawResponse) {
    drawResponse.winners.forEach(winner => {
      if (!this.scores[winner.playerId]) {
        this.scores[winner.playerId] = 0;
      }

      this.scores[winner.playerId] = this.scores[winner.playerId] + 1;
    });
  }

  private decorateWinners(drawResponse: DrawResponse) {
    setTimeout(() => {
      this.winners = drawResponse.winners;
      this.winners.forEach(winner => {
        winner.card = [this.playerCard, this.enemyCard].filter(x => x.id === winner.cardId)[0];
        winner.player = this.game.players.filter(x => x.id === winner.playerId)[0];
      });
    }, 2000);
  }
}
