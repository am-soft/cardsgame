import { TestBed } from '@angular/core/testing';

import { GameService } from './game.service';
import { ApiService } from './api.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import Game from '../models/game';
import CardType from '../models/cardType';
import DrawResponse from '../models/drawResponse';
import AttributeType from '../models/attributeType';

describe('GameService', () => {
  let httpTestingController: HttpTestingController;

  const game: Game = {
    id: 'some-game-id',
    players: [
      { id: 'some-player-id-1', name: 'Test Player 1', gameId: 'some-game-id' },
      { id: 'some-player-id-2', name: 'Test Player 2', gameId: 'some-game-id' }
    ],
    playersCount: 2
  };

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [ApiService], imports: [HttpClientTestingModule] });

    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    const service: GameService = TestBed.get(GameService);
    expect(service).toBeTruthy();
  });

  it('should call POST /game on startGame', () => {
    const service: GameService = TestBed.get(GameService);

    [CardType.Person, CardType.Starship].forEach(cardType => {
      service.startGame(cardType).subscribe(returnedGame => {
        expect(returnedGame).toEqual(game);
        expect(() => service.startGame(cardType)).toThrowError();
        service.game = null;
      });
      const requestMade = httpTestingController.expectOne(ApiService.BaseUrl + 'game');
      requestMade.flush(game);
    });
  });

  it('should call POST /game/{gameId}/draw on player and enemy draw and update scores and winners', () => {
    const service: GameService = TestBed.get(GameService);
    service.game = game;

    const drawResponse1: DrawResponse = {
      gameId: game.id,
      card: {
        attribute: { id: 'some-attribute-id', value: 1, type: AttributeType.Crew },
        cardType: CardType.Starship,
        id: 'some-card-id',
        image: 'some-image',
        name: 'Test Card'
      },
      playerId: game.players[0].id,
      round: 1,
      winners: null
    };

    const drawResponse2: DrawResponse = {
      gameId: game.id,
      card: {
        attribute: { id: 'some-attribute-id', value: 1, type: AttributeType.Crew },
        cardType: CardType.Starship,
        id: 'some-card-id',
        image: 'some-image',
        name: 'Test Card'
      },
      playerId: game.players[1].id,
      round: 1,
      winners: [
        { cardId: 'some-card-id', playerId: game.players[0].id, card: null, player: null },
        { cardId: 'some-card-id', playerId: game.players[1].id, card: null, player: null }
      ]
    };

    service.drawPlayer().subscribe(response => {
      expect(response).toEqual(drawResponse1);
      expect(service.playerCard).toEqual(drawResponse1.card);
    });

    let requestMade = httpTestingController.expectOne(`${ApiService.BaseUrl}game/${game.id}/draw`);
    expect(requestMade.request.method).toBe('POST');
    requestMade.flush(drawResponse1);

    service.drawEnemy().subscribe(response => {
      expect(response).toEqual(drawResponse2);
      expect(service.enemyCard).toEqual(drawResponse2.card);
      expect(service.scores[game.players[0].id]).toBe(1);
      expect(service.scores[game.players[1].id]).toBe(1);
      expect(service.winners.length).toBe(2);
    });

    requestMade = httpTestingController.expectOne(`${ApiService.BaseUrl}game/${game.id}/draw`);
    expect(requestMade.request.method).toBe('POST');
    requestMade.flush(drawResponse2);
  });
});
