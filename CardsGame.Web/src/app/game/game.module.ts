import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiModule } from '../ui/ui.module';
import { MainComponent } from '../game/components/main/main.component';
import { CardComponent } from '../game/components/card/card.component';
import { ResourceSelectorComponent } from './components/resource-selector/resource-selector.component';
import { ResourceSelectorCardComponent } from './components/resource-selector-card/resource-selector-card.component';
import { PlayScreenComponent } from './components/play-screen/play-screen.component';
import { GameService } from './services/game.service';
import { HttpClientModule } from '@angular/common/http';
import { ResultsComponent } from './components/results/results.component';

@NgModule({
  declarations: [
    MainComponent,
    CardComponent,
    ResourceSelectorComponent,
    ResourceSelectorCardComponent,
    PlayScreenComponent,
    ResultsComponent
  ],
  imports: [CommonModule, UiModule, HttpClientModule],
  exports: [MainComponent],
  providers: [GameService]
})
export class GameModule {}
