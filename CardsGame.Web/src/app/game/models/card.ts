import Attribute from './attribute';
import BaseEntity from './baseEntity';
import CardType from './cardType';

export default interface Card extends BaseEntity {
  name: string;
  image: string;
  attribute: Attribute;
  cardType: CardType;
}
