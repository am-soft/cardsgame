import Card from './card';
import Player from './player';

export default interface RoundWinner {
  playerId: string;
  cardId: string;
  player: Player;
  card: Card;
}
