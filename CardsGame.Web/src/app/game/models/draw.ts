import BaseEntity from './baseEntity';

export default interface Draw extends BaseEntity {
  playerId: string;
  cardId: string;
  gameId: string;
  round: number;
  cardValue: number;
}
