import Card from './card';
import RoundWinner from './roundWinner';

export default interface DrawResponse {
  gameId: string;
  playerId: string;
  card: Card;
  round: number;
  winners: RoundWinner[];
}
