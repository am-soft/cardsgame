import BaseEntity from './baseEntity';
import AttributeType from './attributeType';

export default interface Attribute extends BaseEntity {
  type: AttributeType;
  value: number;
}
