import CardType from './cardType';

export default interface DrawRequest {
  playerId: string;
  cardType: CardType;
}
