import BaseEntity from './baseEntity';
import Player from './player';

export default interface Game extends BaseEntity {
  playersCount: number;
  players: Player[];
}
