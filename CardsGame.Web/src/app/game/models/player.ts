import BaseEntity from './baseEntity';

export default interface Player extends BaseEntity {
    name: string;
    gameId: string;
}