import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GameService } from '../../services/game.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {
  constructor(public gameService: GameService) {}

  ngOnInit() {}

  getWinnersLabel() {
    return this.gameService.winners.length === 1 ? 'Winner' : 'Winners';
  }

  getWinners() {
    return this.gameService.winners.map(x => x.player.name).join(', ');
  }

  nextRound() {
    this.gameService.nextRound();
  }
}
