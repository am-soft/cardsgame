import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsComponent } from './results.component';
import { GameModule } from '../../game.module';
import { GameService } from '../../services/game.service';
import AttributeType from '../../models/attributeType';
import CardType from '../../models/cardType';
import { By } from '@angular/platform-browser';

describe('ResultsComponent', () => {
  let component: ResultsComponent;
  let fixture: ComponentFixture<ResultsComponent>;
  let stubGameService: jasmine.SpyObj<GameService>;

  beforeEach(async(() => {
    stubGameService = jasmine.createSpyObj<GameService>(['nextRound']);
    stubGameService.game = {
      id: 'some-game-id',
      players: [
        { id: 'some-player-id-1', name: 'Test Player 1', gameId: 'some-game-id' },
        { id: 'some-player-id-2', name: 'Test Player 2', gameId: 'some-game-id' }
      ],
      playersCount: 2
    };
    stubGameService.playerCard = {
      attribute: { id: 'some-attribute-id-1', type: AttributeType.Crew, value: 1 },
      cardType: CardType.Starship,
      image: 'question-mark.jpg',
      id: 'some-card-id-1',
      name: 'Test Card 1'
    };
    stubGameService.enemyCard = {
      attribute: { id: 'some-attribute-id-2', type: AttributeType.Crew, value: 1 },
      cardType: CardType.Starship,
      image: 'question-mark.jpg',
      id: 'some-card-id-2',
      name: 'Test Card 2'
    };
    stubGameService.scores = {};
    stubGameService.winners = [
      {
        player: stubGameService.game.players[0],
        playerId: stubGameService.game.players[0].id,
        card: stubGameService.playerCard,
        cardId: stubGameService.playerCard.id
      }
    ];

    TestBed.configureTestingModule({
      imports: [GameModule]
    })
      .overrideProvider(GameService, { useValue: stubGameService })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display singular for one winner', () => {
    const h1 = fixture.debugElement.query(By.css('h1'));
    expect(h1.nativeElement.textContent).toBe(`Winner: ${stubGameService.game.players[0].name}`);
  });

  it('should display plural for two winners', () => {
    stubGameService.winners.push({
      card: stubGameService.enemyCard,
      cardId: stubGameService.enemyCard.id,
      player: stubGameService.game.players[1],
      playerId: stubGameService.game.players[1].id
    });

    fixture.detectChanges();

    const h1 = fixture.debugElement.query(By.css('h1'));
    expect(h1.nativeElement.textContent).toBe(
      `Winners: ${stubGameService.game.players[0].name}, ${stubGameService.game.players[1].name}`
    );
  });

  it('should call nextRound on button click', () => {
    spyOn(component, 'nextRound');

    const button = fixture.debugElement.query(By.css('button'));
    button.nativeElement.click();

    expect(component.nextRound).toHaveBeenCalled();
  });
});
