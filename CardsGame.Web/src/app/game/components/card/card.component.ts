import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import Card from '../../models/card';
import AttributeType from '../../models/attributeType';
import { GameService } from '../../services/game.service';
import Player from '../../models/player';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit, OnChanges {
  disableDrawButton = false;

  @Input()
  showCardOnly: boolean;

  @Input()
  cardHolder: Player;

  @Input()
  model: Card;

  @Output()
  onDraw: EventEmitter<{}> = new EventEmitter();

  @Input()
  scores: { [playerId: string]: number };

  constructor() {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['model'].previousValue && !changes['model'].currentValue) {
      this.disableDrawButton = false;
    }
  }

  draw() {
    if (this.model) throw new Error('Trying to roll when already rolled');
    this.onDraw.emit();
    this.disableDrawButton = true;
  }

  getAttributeTypeText() {
    return AttributeType[this.model.attribute.type];
  }

  getScore() {
    const score = this.scores[this.cardHolder.id];
    return score ? score : 0;
  }
}
