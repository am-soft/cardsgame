import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardComponent } from './card.component';
import { GameModule } from '../../game.module';
import AttributeType from '../../models/attributeType';
import CardType from '../../models/cardType';
import { By } from '@angular/platform-browser';

describe('CardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<CardComponent>;
  let cardHolder = { id: 'some-player-id-1', name: 'Test Player 1', gameId: 'some-game-id' };
  let card = {
    attribute: { id: 'some-attribute-id-1', type: AttributeType.Crew, value: 1 },
    cardType: CardType.Starship,
    image: 'question-sign.jpg',
    id: 'some-card-id-1',
    name: 'Test Card 1'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [GameModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardComponent);
    component = fixture.componentInstance;
    component.cardHolder = cardHolder;
    component.model = card;
    component.scores = {};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render card details', () => {
    const debugEl = fixture.debugElement;

    const h1 = debugEl.query(By.css('h1'));
    expect(h1.nativeElement.textContent).toBe(cardHolder.name);

    const img = debugEl.query(By.css('img'));
    expect(img.nativeElement.getAttribute('src')).toContain(card.image);

    const details = debugEl.queryAll(By.css('.details span.value'));
    expect(details[0].nativeElement.textContent).toBe(card.name);
    expect(details[1].nativeElement.textContent).toBe(card.attribute.value.toString());

    const rollButton = fixture.debugElement.query(By.css('mat-card-actions button'));
    expect(rollButton.nativeElement.getAttribute('disabled')).toBeNull();
  });

  it('should not display roll button and score for showCardOnly cards', () => {
    component.showCardOnly = true;
    fixture.detectChanges();

    const rollButton = fixture.debugElement.query(By.css('mat-card-actions button'));
    expect(rollButton).toBeFalsy();

    const score = fixture.debugElement.query(By.css('.score'));
    expect(score).toBeFalsy();
  });

  it('should display roll button and score for not showCardOnly cards', () => {
    component.showCardOnly = false;
    fixture.detectChanges();

    const rollButton = fixture.debugElement.query(By.css('mat-card-actions button'));
    expect(rollButton).toBeTruthy();

    const score = fixture.debugElement.query(By.css('.score'));
    expect(score).toBeTruthy();
  });

  it('should call draw on draw button click', () => {
    spyOn(component, 'draw');

    const drawButton = fixture.debugElement.query(By.css('mat-card-actions button'));
    drawButton.nativeElement.click();

    expect(component.draw).toHaveBeenCalled();
  });

  it('should disable draw button after draw', () => {
    component.model = null;
    component.draw();
    fixture.detectChanges();

    const drawButton = fixture.debugElement.query(By.css('mat-card-actions button'));
    expect(drawButton.nativeElement.getAttribute('disabled')).toBe('true');
  });
});
