import { Component, OnInit } from '@angular/core';
import ResourceType from '../../models/cardType';
import { GameService } from '../../services/game.service';

@Component({
  selector: 'app-resource-selector',
  templateUrl: './resource-selector.component.html',
  styleUrls: ['./resource-selector.component.scss']
})
export class ResourceSelectorComponent implements OnInit {
  availableResources = [ResourceType.Person, ResourceType.Starship];
  selectedResourceType: ResourceType = null;

  constructor(public gameService: GameService) {}

  ngOnInit() {}

  startGame() {
    this.gameService.startGame(this.selectedResourceType);
  }
}
