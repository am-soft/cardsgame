import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceSelectorComponent } from './resource-selector.component';
import { GameService } from '../../services/game.service';
import AttributeType from '../../models/attributeType';
import CardType from '../../models/cardType';
import { GameModule } from '../../game.module';
import { By } from '@angular/platform-browser';

describe('ResourceSelectorComponent', () => {
  let component: ResourceSelectorComponent;
  let fixture: ComponentFixture<ResourceSelectorComponent>;
  let stubGameService: jasmine.SpyObj<GameService>;

  beforeEach(async(() => {
    stubGameService = jasmine.createSpyObj<GameService>(['nextRound', 'startGame']);
    stubGameService.game = {
      id: 'some-game-id',
      players: [
        { id: 'some-player-id-1', name: 'Test Player 1', gameId: 'some-game-id' },
        { id: 'some-player-id-2', name: 'Test Player 2', gameId: 'some-game-id' }
      ],
      playersCount: 2
    };
    stubGameService.playerCard = {
      attribute: { id: 'some-attribute-id-1', type: AttributeType.Crew, value: 1 },
      cardType: CardType.Starship,
      image: 'question-mark.jpg',
      id: 'some-card-id-1',
      name: 'Test Card 1'
    };
    stubGameService.enemyCard = {
      attribute: { id: 'some-attribute-id-2', type: AttributeType.Crew, value: 1 },
      cardType: CardType.Starship,
      image: 'question-mark.jpg',
      id: 'some-card-id-2',
      name: 'Test Card 2'
    };

    TestBed.configureTestingModule({
      imports: [GameModule]
    })
      .overrideProvider(GameService, { useValue: stubGameService })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render two resource selector cards', () => {
    const cards = fixture.debugElement.queryAll(By.css('app-resource-selector-card'));
    expect(cards.length).toBe(2);
  });

  it('should call startGame after button click', () => {
    spyOn(component, 'startGame');

    const button = fixture.debugElement.query(By.css('button'));
    button.nativeElement.click();

    expect(component.startGame).toHaveBeenCalled();
  });

  it('should call gameService.startGame on startGame', () => {
    component.startGame();
    expect(stubGameService.startGame).toHaveBeenCalled();
  });

  it('should mark card as selected after click', () => {
    const cards = fixture.debugElement.queryAll(By.css('app-resource-selector-card'));

    cards.forEach(card => {
      card.nativeElement.click();
      fixture.detectChanges();

      const selectedResource = card.componentInstance.resourceType;
      expect(component.selectedResourceType).toBe(selectedResource);
    });
  });
});
