import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayScreenComponent } from './play-screen.component';
import { GameModule } from '../../game.module';
import { GameService } from '../../services/game.service';
import AttributeType from '../../models/attributeType';
import CardType from '../../models/cardType';
import { By } from '@angular/platform-browser';

describe('PlayScreenComponent', () => {
  let component: PlayScreenComponent;
  let fixture: ComponentFixture<PlayScreenComponent>;
  let stubGameSerive: jasmine.SpyObj<GameService>;

  beforeEach(async(() => {
    stubGameSerive = jasmine.createSpyObj<GameService>(['drawPlayer', 'drawEnemy', 'nextRound']);
    stubGameSerive.game = {
      id: 'some-game-id',
      players: [
        { id: 'some-player-id-1', name: 'Test Player 1', gameId: 'some-game-id' },
        { id: 'some-player-id-2', name: 'Test Player 2', gameId: 'some-game-id' }
      ],
      playersCount: 2
    };
    stubGameSerive.playerCard = {
      attribute: { id: 'some-attribute-id-1', type: AttributeType.Crew, value: 1 },
      cardType: CardType.Starship,
      image: 'question-mark.jpg',
      id: 'some-card-id-1',
      name: 'Test Card 1'
    };
    stubGameSerive.enemyCard = {
      attribute: { id: 'some-attribute-id-2', type: AttributeType.Crew, value: 1 },
      cardType: CardType.Starship,
      image: 'question-mark.jpg',
      id: 'some-card-id-2',
      name: 'Test Card 2'
    };
    stubGameSerive.scores = {};

    TestBed.configureTestingModule({
      imports: [GameModule]
    })
      .overrideProvider(GameService, { useValue: stubGameSerive })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render two card components', () => {
    const cards = fixture.debugElement.queryAll(By.css('app-card'));
    expect(cards.length).toBe(2);
    expect(cards[0].componentInstance.model).toEqual(stubGameSerive.playerCard);
    expect(cards[0].componentInstance.cardHolder).toEqual(stubGameSerive.game.players[0]);
    expect(cards[1].componentInstance.model).toEqual(stubGameSerive.enemyCard);
    expect(cards[1].componentInstance.cardHolder).toEqual(stubGameSerive.game.players[1]);
  });

  it('should render results when round finished', () => {
    let results = fixture.debugElement.query(By.css('app-results'));
    expect(results).toBeFalsy();

    stubGameSerive.winners = [
      {
        card: stubGameSerive.playerCard,
        cardId: stubGameSerive.playerCard.id,
        player: stubGameSerive.game.players[0],
        playerId: stubGameSerive.game.players[0].id
      }
    ];

    fixture.detectChanges();

    results = fixture.debugElement.query(By.css('app-results'));
    expect(results).toBeTruthy();
  });

  it('should call gameService draw methods on draw', () => {
    component.drawPlayer();
    expect(stubGameSerive.drawPlayer).toHaveBeenCalled();

    component.drawEnemy();
    expect(stubGameSerive.drawEnemy).toHaveBeenCalled();
  });
});
