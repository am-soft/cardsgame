import { Component, OnInit } from '@angular/core';
import { GameService } from '../../services/game.service';

@Component({
  selector: 'app-play-screen',
  templateUrl: './play-screen.component.html',
  styleUrls: ['./play-screen.component.scss']
})
export class PlayScreenComponent implements OnInit {
  constructor(public gameService: GameService) {}

  ngOnInit() {}

  drawPlayer() {
    this.gameService.drawPlayer();
  }

  drawEnemy() {
    this.gameService.drawEnemy();
  }
}
