import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainComponent } from './main.component';
import { GameModule } from '../../game.module';
import { GameService } from '../../services/game.service';
import { By } from '@angular/platform-browser';

describe('MainComponent', () => {
  let component: MainComponent;
  let fixture: ComponentFixture<MainComponent>;
  let stubGameService: jasmine.SpyObj<GameService>;

  beforeEach(async(() => {
    stubGameService = jasmine.createSpyObj<GameService>(['draw']);
    stubGameService.scores = {};

    TestBed.configureTestingModule({
      imports: [GameModule]
    })
      .overrideProvider(GameService, { useValue: stubGameService })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render resource selector when game not started', () => {
    stubGameService.game = null;
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('app-resource-selector'))).toBeTruthy();
    expect(fixture.debugElement.query(By.css('app-play-screen'))).toBeFalsy();
  });

  it('should render play screen when game started', () => {
    stubGameService.game = {
      id: 'some-game-id',
      players: [
        { id: 'some-player-id-1', name: 'Test Player 1', gameId: 'some-game-id' },
        { id: 'some-player-id-2', name: 'Test Player 2', gameId: 'some-game-id' }
      ],
      playersCount: 2
    };
    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('app-play-screen'))).toBeTruthy();
    expect(fixture.debugElement.query(By.css('app-resource-selector'))).toBeFalsy();
  });
});
