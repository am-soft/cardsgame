import { Component, OnInit, Input } from '@angular/core';
import ResourceType from '../../models/cardType';

@Component({
  selector: 'app-resource-selector-card',
  templateUrl: './resource-selector-card.component.html',
  styleUrls: ['./resource-selector-card.component.scss']
})
export class ResourceSelectorCardComponent implements OnInit {
  @Input()
  resourceType: ResourceType;

  @Input()
  selected: boolean;

  constructor() {}

  ngOnInit() {}

  getResourceTitle() {
    switch (this.resourceType) {
      case ResourceType.Person:
        return 'Person';
      case ResourceType.Starship:
        return 'Starship';
    }
  }

  getResourceImage() {
    switch (this.resourceType) {
      case ResourceType.Person:
        return '../../../assets/person.png';
      case ResourceType.Starship:
        return '../../../assets/starship.png';
    }
  }
}
