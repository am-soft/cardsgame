import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceSelectorCardComponent } from './resource-selector-card.component';
import { GameModule } from '../../game.module';

describe('ResourceSelectorCardComponent', () => {
  let component: ResourceSelectorCardComponent;
  let fixture: ComponentFixture<ResourceSelectorCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [GameModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceSelectorCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
