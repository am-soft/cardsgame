import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule, MatCardModule } from '@angular/material';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  exports: [FlexLayoutModule, MatButtonModule, MatIconModule, MatCardModule]
})
export class UiModule {}
